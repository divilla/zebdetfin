package main

import (
	"fmt"
	"gitlab.com/divilla/zebdetfin/internal/config"
	"gitlab.com/divilla/zebdetfin/internal/process"
	"gitlab.com/divilla/zebdetfin/pkg/graph"
	"go.uber.org/zap"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"time"
)

func main() {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	cfg := config.NewConfig("config/config.yaml")

	g := graph.NewGraph(logger, cfg)
	g.Init()

	p := message.NewPrinter(language.English)
	go func(g *graph.Graph, p *message.Printer) {
		for {
			<-time.After(time.Duration(g.Tick()) * time.Second)
			_, _ = p.Printf("\r\nTried: %d combinations\r\n", g.Counter())
		}
	}(g, p)

	if cfg.Warnsdorff {
		if out, ok := process.Warnsdorff(g); ok {
			fmt.Println(out)
		}
	} else {
		if out, ok := process.DFS(g); ok == true {
			fmt.Println("---------------------------------------------------- Solution -------------------------------------------------------------------")
			fmt.Println(out)
		}
	}
}
