package config

import (
	"gopkg.in/yaml.v3"
	"io/ioutil"
)

type Config struct {
	BoardSize        [2]int   `yaml:"board_size"`
	StartingPosition [2]int   `yaml:"starting_position"`
	Moves            [][2]int `yaml:"moves"`
	Warnsdorff       bool     `yaml:"warnsdorff"`
	TickSeconds      int      `yaml:"tick_seconds"`
	LogFailed        bool     `yaml:"log_failed"`
}

func NewConfig(path string) *Config {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	var config Config
	if err = yaml.Unmarshal(file, &config); err != nil {
		panic(err)
	}

	return &config
}
